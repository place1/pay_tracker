from django.conf.urls import url
from app_frontend import views

urlpatterns = [
    url(r'^$', views.Index.as_view())
]