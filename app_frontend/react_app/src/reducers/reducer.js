import {
	GET_INVOICES,
	CREATE_INVOICE,
} from 'actions/types';

const initialState = {
	invoices: [],
}

function reducer(state = initialState, action) {
	switch(action.type) {
		case GET_INVOICES:
			return Object.assign({}, state, {
				invoices: action.invoices
			});

		case CREATE_INVOICE:
			return Object.assign({}, state, {
				invoices: [
					...state.invoices,
					action.invoice
				]
			});

		default:
			return state;
	}
}

export default reducer;
