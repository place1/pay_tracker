import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, hashHistory } from 'react-router';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import reducer from 'reducers/reducer';
import getCookie from 'util/getCookie';

import BaseLayout from 'layouts/BaseLayout';
import ViewInvoices from 'views/ViewInvoices';
import ViewInvoice from 'views/ViewInvoice';
import CreateInvoice from 'views/CreateInvoice';

const store = createStore(reducer);

// setup global ajax listener to auto add the CSRF_TOKEN header.
$(document).ajaxSend(function(elm, xhr, s) {
	function csrfSafeMethod(method) {
		// these HTTP methods do not require CSRF protection
		return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
	}
	if (!csrfSafeMethod(s.type.toUpperCase()) && !xhr.crossDomain) {
		xhr.setRequestHeader('X-CSRFToken', getCookie('csrftoken'));
	}
});

render(<Provider store={store}>
		<Router history={hashHistory}>
			<Route path="/" component={BaseLayout}>
				<IndexRoute component={ViewInvoices}/>
			</Route>
			<Route path="/create-invoice" component={BaseLayout}>
				<IndexRoute component={CreateInvoice}/>
			</Route>
			<Route path="/invoice/:id" component={BaseLayout}>
				<IndexRoute component={ViewInvoice}/>
			</Route>
		</Router>
	</Provider>, document.getElementById('App'));
