import React, {PropTypes} from 'react';
import { connect } from 'react-redux'
import CostTable from 'components/CostTable';

function mapStateToProps(state, props) {
	return {
		invoice: state.invoices.find((inv) => {
			if (inv.id == props.params.id) {
				return inv;
			}
		})
	}
}

function mapDispatchToProps(dispatch) {
	return {

	}
}

@connect(mapStateToProps, mapDispatchToProps)
class ViewInvoice extends React.Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {
		params: PropTypes.shape({
			id: PropTypes.string.isRequired,
		}).isRequired,
		invoice: PropTypes.object,
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-12">
						{(() => {
							if (!this.props.invoice) {
								return "invoice not found."
							}
							else {
								let invoice = this.props.invoice;
								return (
									<div>
										<small>invoice {invoice.id}</small>
										<h1>{invoice.billFrom.name}</h1>
										<h3>Date Due: </h3><strong>{invoice.paymentDue}</strong>
										<h3>Bill To: </h3><strong>{invoice.billTo.name}</strong>
										<h3>For: </h3><strong>{invoice.serice}</strong>
										<CostTable items={invoice.item_set}/>
									</div>
								)
							}
						})()}
					</div>
				</div>
			</div>
		);
	}
}

export default ViewInvoice
