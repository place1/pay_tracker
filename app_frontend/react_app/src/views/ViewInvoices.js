import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import InvoiceRow from 'components/InvoiceRow';
import { Link } from 'react-router';
import 'styles/ViewInvoices.css';

// action imports
import { getInvoices } from 'actions/actions';

function mapStateToProps(state) {
	return {
		invoices: state.invoices
	}
}

function mapDispatchToProps(dispatch) {
	return {
		getInvoices: () => {
			$.ajax({
				url: "/api/invoice/",
				type: "get",
				dataType: 'json',
				success: (data) => dispatch(getInvoices(data)),
				error: () => console.log('error getting invoices'),
			});
		}
	}
}

@connect(mapStateToProps, mapDispatchToProps)
class Index extends React.Component {
	constructor(props) {
		super(props);
	}

	componentDidMount() {
		this.props.getInvoices();
	}

	static propTypes = {
		invoices: PropTypes.array.isRequired,
		getInvoices: PropTypes.func.isRequired,
	}

	static contextTypes = {
		router: React.PropTypes.object.isRequired // for react router this.context.router
	}

	invoiceClick(invoice, event) {
		this.context.router.push(`/invoice/${invoice.id}`)
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-12">
					{(() => {
						if (this.props.invoices.length > 0) {
							return (
								<table width="100%" className="table table-striped">
									<thead className="ViewInvoice-thead">
										<tr>
											{Object.keys(this.props.invoices[0]).map((key) => {
												return (
													<td key={key}><strong>{key}</strong></td>
												)
											})}
										</tr>
									</thead>
									<tbody className="ViewInvoice-tbody">
										{this.props.invoices.map((invoice) => {
											return (
												<InvoiceRow key={invoice.id} onClick={this.invoiceClick.bind(this, invoice)} invoice={invoice}/>
											);
										})}
									</tbody>
								</table>
							)
						}
						else {
							return (
								<div className="alert alert-primary">
									<strong>There are no invoices</strong>
								</div>
							)
						}
					})()}
					</div>
				</div>
			</div>
		);
	}
}

export default Index
