import React, {PropTypes} from 'react';
import { connect } from 'react-redux';
import 'util/serializejson';

function mapStateToProps(state) {
	return {

	}
}

function mapDispatchToProps(dispatch) {
	return {
		createInvoice: (invoiceData) => {
			$.ajax({
				url: '/api/invoice/',
				type: 'post',
				data: invoiceData,
				success: (data) => dispatch(data),
				error: (xhr) => console.log(xhr.responseText)
			});
		}
	}
}

@connect(mapStateToProps, mapDispatchToProps)
class CreateInvoice extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			businessEntities: []
		};
		$.ajax({
			url: "/api/businessEntity/",
			type: 'get',
			contentType: 'json',
			success: (data) => this.setState({businessEntities: data}),
			error: (xhr) => console.log(xhr.responseText)
		});
	}

	static propTypes = {
		createInvoice: PropTypes.func.isRequired,
	}

	createInvoice(e) {
		e.preventDefault();
		var data = $(this._invoiceForm).serializeJSON();
		this.props.createInvoice(data);
	}

	getBEOptions() {
		return this.state.businessEntities.map((b) => {
			return (
				<option key={b.id} value={b.id}>{b.name}</option>
			);
		});
	}

	render() {
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-md-9">
						<form ref={(x) => this._invoiceForm = x} onSubmit={this.createInvoice.bind(this)} className="form-horizontal">
							<div className="control-group form-group">
								<label>Bill To</label>
								<select name="billTo" className="form-control">
									<option value="">choose</option>
									{this.getBEOptions()}
								</select>
							</div>
							<div className="control-group form-group">
								<label>Bill From</label>
								<select name="billFrom" className="form-control">
									<option value="">choose</option>
									{this.getBEOptions()}
								</select>
							</div>
							<div className="control-group form-group">
								<label>Service</label>
								<input name="service" type="text" placeholder="service" className="form-control"/>
							</div>
							<div className="control-group form-group">
								<label>Status</label>
								<select name="status" className="form-control" required>
									<option value=""/>
									<option value="DRAFT">draft</option>
									<option value="SENT">sent</option>
									<option value="PAID">paid</option>
									<option value="PARTIAL">partial</option>
									<option value="OVERDUE">overdue</option>
								</select>
							</div>
							<div className="control-group form-group">
								<label>Sent Date</label>
								<input name="invoiceSentDate" type="date" className="form-control"/>
							</div>
							<div className="control-group form-group">
								<label>Due Date</label>
								<input name="paymentDueDate" type="date" className="form-control"/>
							</div>
							<div className="control-group form-group">
								<button type="submit" className="btn btn-primary pull-right">create</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}
}

export default CreateInvoice
