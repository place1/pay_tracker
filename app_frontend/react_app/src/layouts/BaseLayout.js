import React, {PropTypes} from 'react';
import Sidebar from 'components/Sidebar';

class BaseLayout extends React.Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {

	}

	render() {
		return (<div>
			<div className="container-fluid">
				<div className="row">
					<div className="col-xs-3" style={{padding: 0, minHeight: "100vh"}}>
						<Sidebar/>
					</div>
					<div className="col-xs-9" style={{padding: 0}}>
						{this.props.children}
					</div>
				</div>
			</div>
		</div>);
	}
}

export default BaseLayout;
