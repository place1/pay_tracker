import {
	GET_INVOICES,
	CREATE_INVOICE,
} from 'actions/types';

function getInvoices(invoices) {
	return {
		type: GET_INVOICES,
		invoices: invoices
	}
}

function createInvoice(invoice) {
	return {
		type: CREATE_INVOICE,
		invoice: invoice,
	}
}

export {
	getInvoices,
	createInvoice,
}; // not default export as more actions are to be added
