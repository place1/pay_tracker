import React, {PropTypes} from 'react';

class CostTable extends React.Component {
	constructor(props) {
		super(props);
		this.total = 0.0;
		for (var i = 0; i < this.props.items.length; i++) {
			this.total += parseFloat(this.props.items[i].cost)
		}
	}

	static propTypes = {
		items: PropTypes.arrayOf(PropTypes.shape({
			description: PropTypes.string.isRequired,
			cost: PropTypes.string.isRequired,
		})),
	}

	render() {
		return (
			<div>
				<table width="100%">
					<thead>
						<tr>
							<td>description</td>
							<td>cost</td>
						</tr>
					</thead>
					<tbody>
						{(() => {
							return this.props.items.map((item, i) => {
								return (
									<tr key={i}>
										<td>{item.description}</td>
										<td>{item.unitCost}</td>
									</tr>
								)
							});
						})()}
					</tbody>
				</table>
				<table>
					<thead></thead>
					<tbody>
						<tr>
							<td>Total</td>
							<td>{this.total}</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}

export default CostTable
