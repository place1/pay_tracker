import React, {PropTypes} from 'react';
import 'styles/InvoiceRow.css';

class InvoiceRow extends React.Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {
		invoice: PropTypes.object.isRequired,
		onClick: PropTypes.func,
	}

	render() {
		return (
			<tr onClick={this.props.onClick} className="invoice-row-container">
				{(() => {
					return Object.keys(this.props.invoice).map((key) => {
						return (
							<td key={key}>
								{(() => {
									var item = this.props.invoice[key];
									if (item instanceof Object) {
										return item.name || item.username;
									}
									else {
										return item;
									}
								})()}
							</td>
						);
					});
				})()}
			</tr>
		);
	}
}

export default InvoiceRow
