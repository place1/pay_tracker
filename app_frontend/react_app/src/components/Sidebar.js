import React, {PropTypes} from 'react';
import { Link } from 'react-router';
import 'styles/Sidebar.css';

class Sidebar extends React.Component {
	constructor(props) {
		super(props);
	}

	static propTypes = {

	}

	render() {
		return (
			<div className="nav-container container-fluid">
				<nav>
					<div className="brand">
						<h2>pay tracker</h2>
					</div>
					<hr/>
					<ul className="links">
						<li>
							<Link to="/"><i className="fa fa-book"></i> View Invoices</Link>
						</li>
						<li>
							<Link to="/create-invoice/"><i className="fa fa-book"></i> Create Invoice</Link>
						</li>
					</ul>
				</nav>
			</div>
		);
	}
}

export default Sidebar
