var path = require("path");
var webpack = require('webpack');

module.exports = {
	context: path.join(__dirname, "src"),
	entry: "./Main.js",
	output: {
		path: path.join(__dirname, "static", "dist"),
		filename: "App.js"
	},
	resolve: {
		root: [
			path.join(__dirname, "src"),
		]
	},
	plugins: [],
	module: {
		loaders: [{
			test: /\.jsx?$/,
			exclude: /node_modules/,
			loader: "babel-loader",
			query: {
				presets: ['es2015', 'react', 'stage-0'],
				plugins: ['transform-decorators-legacy']
			}
		}, {
			test: /\.css$/,
			loader: "style-loader!css-loader",
		}]
	}
};
