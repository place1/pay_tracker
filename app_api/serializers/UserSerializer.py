from rest_framework import serializers
from app_api import models

class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User  # django auth model
        fields = ('id', 'username',)
        read_only_fields = ('id',)