from rest_framework import serializers
from app_api import models

class ContactInfoSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.ContactInfo
        fields = '__all__'