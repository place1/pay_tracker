from rest_framework import serializers

class ExpandableSerializer(serializers.ModelSerializer):

    class Meta:
        expandQuery = "expand"
        abstract = True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not hasattr(self.Meta, "expandQuery"):
            setattr(self.Meta, "expandQuery", "expand")  # set default expandQuery string if it's not defined

    def to_representation(self, instance):
        """
        Function overrides normal behaviour of ModelSerializer.

        this will allow request context containing query param 'expand' to make the
        serializer represent nested relationships as either an ID (non-expanded) or
        as a nested object (expanded).

        This behaviour relies on Django rest's APIViews passing 'request' as context to
        the serializer.
        """
        representation = super().to_representation(instance)
        if self.parent is None:
            requestContext = self.context.get('request')
            if requestContext is not None:
                expandFields = requestContext.query_params.getlist(self.Meta.expandQuery)
                for key in representation.keys():
                    if isinstance(representation[key], dict) and representation[key].get('id') is not None:
                        if key not in expandFields:
                            representation[key] = representation[key]['id']
                    elif isinstance(representation[key], list) and key not in expandFields:
                        representation[key] = map(lambda x: x['id'] if isinstance(x, dict) and 'id' in x else x, representation[key])
        return representation
