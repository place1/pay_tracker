from app_api import models
from .ContactInfoSerializer import ContactInfoSerializer
from .RelationalModelSerializer import RelationalModelSerializer

class BusinessEntitySerializer(RelationalModelSerializer):
    contactInfo = ContactInfoSerializer()

    class Meta:
        model = models.BusinessEntity
        fields = '__all__'
