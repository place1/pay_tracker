from rest_framework import serializers
from rest_framework.serializers import ValidationError
from rest_framework.fields import empty
from django.db import transaction
from django.core import exceptions


class RelationalModelSerializer(serializers.ModelSerializer):

    class Meta:
        abstract = True

    def __init__(self, instance=None, data=empty, **kwargs):
        self.is_relation = kwargs.pop('is_relation', False)
        super().__init__(instance, data, **kwargs)

    def validate_empty_values(self, data):
        """
        This overrides the normal behaviour of ModelSerializer.

        If the field has been defined as a relation i.e. 'field = RelationModelSerializer(is_relation=True)'
        then this method handles the logic for making the nested field writable.

        arg: Data;
        if 'data' is a dictionary it will be interpreted as either:
            - linking a model to the parent serializer via and ID. {"id": 1}.
            - doing a nested create using the dictionary as the data for the nested object.
        if 'data' is an integer or string representing an integer:
            - then 'data' will be interpreted as a primary key to link the parent serializer to.
        """
        if self.is_relation: # todo: perhaps replace this with 'if self.parent is not None'
            model = getattr(self.Meta, 'model')
            model_pk = model._meta.pk.name

            if data is serializers.empty and self.root.partial is True:
                # if the field is empty but this is a nested serializer of another that is being updated (partial=True)
                # then we can skip this field.
                raise serializers.SkipField

            if isinstance(data, dict):
                return self.nested_object_validation(data)

            if isinstance(data, (int, str)):
                if isinstance(data, str) and not data.isdigit():  # todo: perhaps we don't need this check
                    raise ValidationError('pk field was a string but not a valid integer')
                return self.nested_pk_validation(data)

            raise ValidationError('invalid data. should be dict, int or str')

        # fallback for non-relation fields.
        return super().validate_empty_values(data)

    def nested_object_validation(self, data): # todo i feel like this could have better design.
        assert(isinstance(data, dict))

        pkName = self.Meta.model._meta.pk.name
        if pkName in data:
            # nested link
            try:
                instance = self.Meta.model.objects.get(pk=data[pkName])
            except self.Meta.model.DoesNotExist:
                raise ValidationError('nested model not found by id=' + str(data[pkName]))
            return True, instance

        # recursive nested create
        validated_data = data
        for field in self._writable_fields:
            if isinstance(field, serializers.BaseSerializer):
                nestedData = data.pop(field.field_name)
                nestedSerializer = field.__class__(data=nestedData)
                nestedSerializer.is_valid(raise_exception=True)
                data[field.field_name] = nestedSerializer
        self._validated_data = validated_data  # this is a problem cuz it skips the is valid pattern
        self._errors = []  # this is a problem cuz it skips the is valid pattern
        return True, self

    def nested_pk_validation(self, data):
        assert(isinstance(data, (int, str)))
        # handle linking a nested model via primary key
        try:
            instance = self.Meta.model.objects.get(pk=data)
        except self.Meta.model.DoesNotExist:
            raise ValidationError('nested model not found by id=' + str(data))
        return True, instance

    def clean_nested_serializers_from_validated_data(self, validated_data):
        items = validated_data.items()
        for field, value in items:
            if isinstance(value, serializers.BaseSerializer):
                # if an item in validated_data is a nested serializer. save it and replace
                # the value with the save result
                validated_data[field] = value.save()  # recursively save all the nested serializers

    def create(self, validated_data):
        # if parent serializer -> start transaction
        with transaction.atomic():  # probably not great because the child serializers will run with a new transaction
            self.clean_nested_serializers_from_validated_data(validated_data)
            instance = super().create(validated_data)
            # if parent serializer -> end transaction
        return instance

    def update(self, instance, validated_data):
        with transaction.atomic():  # probably not great because the child serializers will run with a new transaction
            self.clean_nested_serializers_from_validated_data(validated_data)
            instance = super().update(instance, validated_data)
            # if parent serializer -> end transaction
        return instance
