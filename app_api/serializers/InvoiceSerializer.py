from rest_framework import serializers
from app_api import models
from .UserSerializer import UserSerializer
from .BusinessEntitySerializer import BusinessEntitySerializer
from .UserSerializer import UserSerializer
from .ItemSerializer import ItemSerializer
from .RelationalModelSerializer import RelationalModelSerializer
from .ExpandableSerializer import ExpandableSerializer

class InvoiceSerializer(RelationalModelSerializer, ExpandableSerializer):
    createdBy = UserSerializer(read_only=True)
    billTo = BusinessEntitySerializer(is_relation=True, read_only=False)
    billFrom = BusinessEntitySerializer(is_relation=True, read_only=False)
    item_set = ItemSerializer(many=True)

    class Meta:
        model = models.Invoice
        fields = '__all__'
        read_only_fields = ('createdBy',)
