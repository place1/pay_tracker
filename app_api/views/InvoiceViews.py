from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from app_api.serializers import InvoiceSerializer
from app_api import models

class InvoiceViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = InvoiceSerializer

    def get_queryset(self):
        return models.Invoice.objects.filter(createdBy=self.request.user)

    def perform_create(self, serializer):
        serializer.save(createdBy=self.request.user)
