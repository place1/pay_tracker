from rest_framework import viewsets
from rest_framework import permissions
from app_api import models
from app_api import serializers

class ItemViewSet(viewsets.ModelViewSet):
    queryset = models.Item.objects.all()
    serializer_class = serializers.ItemSerializer
    permission_classes = [permissions.IsAuthenticated]