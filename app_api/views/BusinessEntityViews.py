from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated
from app_api.serializers import BusinessEntitySerializer
from app_api import models

class BusinessEntityViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticated,)
    serializer_class = BusinessEntitySerializer
    queryset = models.BusinessEntity.objects.all()