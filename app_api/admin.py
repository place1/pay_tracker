from django.contrib import admin
from app_api import models

class ItemInline(admin.TabularInline):
    model = models.Item

@admin.register(models.Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = ('id', 'billTo', 'billFrom', 'service', 'status', 'invoiceSentDate', 'paymentDueDate')
    inlines = [ItemInline]

@admin.register(models.BusinessEntity)
class BusinessEntityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')

@admin.register(models.ContactInfo)
class ContactInfoAdmin(admin.ModelAdmin):
    list_display = ('id', 'mobile', 'phone', 'email')