from django.conf.urls import url, include
from rest_framework.routers import SimpleRouter
from app_api import views

router = SimpleRouter()
router.register(r'invoice', viewset=views.InvoiceViewSet, base_name='invoice')
router.register(r'businessEntity', viewset=views.BusinessEntityViewSet, base_name='businessEntity')
router.register(r'item', viewset=views.InvoiceViewSet, base_name='item')

urlpatterns = router.urls

urlpatterns += [
    url(r'^docs/', include('rest_framework_swagger.urls')),
]