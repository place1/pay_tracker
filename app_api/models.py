from django.db.models import *
from django.contrib.auth.models import User

class ContactInfo(Model):
    phone = CharField(max_length=254, blank=True)
    mobile = CharField(max_length=254, blank=True)
    email = CharField(max_length=254, blank=True)
    def __str__(self):
        return "{0} - {1} - {2}".format(self.phone, self.mobile, self.email)


class BusinessEntity(Model):
    user = ForeignKey(User, blank=True, null=True)  # optionally allow an entity to be a user of the site
    name = CharField(max_length=254, blank=False)
    contactInfo = ForeignKey(ContactInfo, blank=True, null=True)

    def __str__(self):
        return self.name


class Invoice(Model):
    createdBy = ForeignKey(User, blank=False)
    billTo = ForeignKey(BusinessEntity, related_name="invoice_bill_to", verbose_name="bill to")
    billFrom = ForeignKey(BusinessEntity, related_name="invoice_bill_from", verbose_name="bill from")
    service = CharField(max_length=254, blank=True, null=True)
    status = CharField(choices=(
        ("DRAFT", "draft (not sent)"),
        ("SENT", "sent"),
        ("PAID", "paid"),
        ("PARTIAL", "partially paid"),
        ("OVERDUE", "overdue"),
    ), blank=False, max_length=254)
    invoiceSentDate = DateField(blank=True, null=True, verbose_name="invoice sent date")
    paymentDueDate = DateField(blank=True, null=True, verbose_name="invoice due date")

    @property
    def daysToPay(self):
        return (self.paymentDueDate - self.invoiceSentDate).days


class Item(Model):
    invoice = ForeignKey(Invoice, blank=False)
    description = CharField(max_length=254)
    amount = IntegerField(default=1, blank=False)
    unitCost = DecimalField(max_digits=10, decimal_places=4, blank=False)