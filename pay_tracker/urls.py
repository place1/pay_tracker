from django.conf.urls import url, include
from django.contrib import admin
from app_api import urls as apiUrls
from app_frontend import urls as frontendUrls

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]

urlpatterns += [
    url(r'^api/', include(apiUrls.urlpatterns)),
    url(r'^', include(frontendUrls.urlpatterns))
]